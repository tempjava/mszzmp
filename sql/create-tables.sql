drop table if exists USERS;
create table USERS
(
    USER_ID integer primary key,
    LOGIN TEXT NOT NULL UNIQUE,
    PASSWORD TEXT NOT NULL ON CONFLICT FAIL,
    FIRST_NAME TEXT,
    LAST_NAME TEXT,
    PHONE TEXT,
    ROLE integer
);

drop table if exists ORDERS;
create table ORDERS
(
    ORDER_ID integer primary key,
    CLIENT_ID integer,
    MECHANIC_ID integer,
    COMPONENT int,
    ISSUE int,
    APPOINTMENT_DATE int
);
