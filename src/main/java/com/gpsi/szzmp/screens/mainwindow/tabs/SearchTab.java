package com.gpsi.szzmp.screens.mainwindow.tabs;

import com.gpsi.szzmp.db.DBConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

public class SearchTab extends Tab {
    private JPanel panel1;
    private JTextField searchPhoneField;
    private JTextField searchNameField;
    private JTextField searchSurnameField;
    private JTextField searchIdField;
    private JButton searchButton;

    public SearchTab() {
        setContentPane(panel1); // wybranie ramki z zawartością
        pack();

        searchButton.addActionListener(searchButtonListener);
    }

    private SearchTab.SearchButtonListener searchButtonListener = new SearchTab.SearchButtonListener();

    class SearchButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            try {
                Statement statement = DBConnection.connection.createStatement();

                String query = "SELECT FIRST_NAME, LAST_NAME, PHONE FROM USERS WHERE " +
                        "FIRST_NAME = '" + searchNameField.getText() + "', " +
                        "LAST_NAME = '" + searchSurnameField.getText() + "', " +
                        "PHONE = '" + searchPhoneField.getText() + "', " + "'";
                System.out.println(query);
                statement.executeUpdate(query);

            } catch (Exception exception) {
                System.out.println(exception);
            }
        }

        private void clearSearchInfoFields() {
            searchNameField.setText("");
            searchSurnameField.setText("");
            searchPhoneField.setText("");
            searchIdField.setText("");
        }
    }

    private void createUIComponents() {
        panel1 = new JPanel();
    }
}
