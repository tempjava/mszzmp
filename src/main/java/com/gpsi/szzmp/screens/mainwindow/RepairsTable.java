package com.gpsi.szzmp.screens.mainwindow;

import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.logic.IssueMapper;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.awt.*;

// http://docs.oracle.com/javase/tutorial/uiswing/components/table.html
// -> TableRenderDemo.java

public class RepairsTable extends JPanel {

    public RepairsTableModel getModel() {
        return model;
    }

    private static Object[][] fetchData() {
        Object[][] data = {{0, "-", "-", "-"}};

        try {
            Statement statement = DBConnection.connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT COUNT(ORDER_ID), " +
                    "COMPONENT, ISSUE, CLIENT_ID, MECHANIC_ID " +
                    "FROM ORDERS " +
                    "GROUP BY COMPONENT, ISSUE " +
                    "ORDER BY COUNT(ORDER_ID) DESC");

            List<Object[]> list = new ArrayList<>();

            while (resultSet.next()) {
                list.add(new Object[]{
                        resultSet.getInt(1),
                        IssueMapper.getComponentTypeString(resultSet.getInt(2)),
                        IssueMapper.getIssueTypeString(resultSet.getInt(2), resultSet.getInt(3)),
                        "Zam 1"
                });
            }

            data = new Object[list.size()][4];

            int i = 0;
            for (Object[] row : list) {
                data[i++] = row;
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }

        return data;
    }

    private RepairsTableModel model;

    public RepairsTable() {
        super(new GridLayout(1,0));

        model = new RepairsTableModel();

        JTable table = new JTable(model);
        //table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);

        setUpOrderColumn(table, table.getColumnModel().getColumn(3));

        add(scrollPane);
    }

    public void setUpOrderColumn(JTable table,
                                 TableColumn orderColumn) {
        //Set up the editor for the sport cells.
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Zam 1");
        comboBox.addItem("Zam 2");
        comboBox.addItem("Zam 3");
        comboBox.addItem("Zam 4");
        comboBox.addItem("Zam 5");
        comboBox.addItem("Zam 6");
        orderColumn.setCellEditor(new DefaultCellEditor(comboBox));

        //Set up tool tips for the sport cells.
        DefaultTableCellRenderer renderer =
                new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        orderColumn.setCellRenderer(renderer);
    }

    public class RepairsTableModel extends AbstractTableModel {
        private String[] columnNames = {
                "Ilość wystąpień",
                "Część pojazdu",
                "Problem",
                "Zamówienie"};
        private Object[][] data = RepairsTable.fetchData();

        public final Object[] longValues = {0, "-", "-", "-"};

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        public boolean isCellEditable(int row, int col) {
            return col == 3;
        }

        public void refreshValues() {
            data = RepairsTable.fetchData();
            fireTableDataChanged();
        }
    }
}