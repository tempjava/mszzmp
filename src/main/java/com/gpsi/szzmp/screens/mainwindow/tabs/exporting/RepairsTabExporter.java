package com.gpsi.szzmp.screens.mainwindow.tabs.exporting;


import com.gpsi.szzmp.screens.mainwindow.MainWindow;
import com.gpsi.szzmp.screens.mainwindow.RepairsTable;
import com.gpsi.szzmp.utils.htmlbuilder.HTMLBuilder;

public class RepairsTabExporter implements TabExporter {
    @Override
    public void export() {
        System.out.println("Eksportowanie widoku napraw...");

        RepairsTable.RepairsTableModel repairsTableModel =
                MainWindow.getInstance().getRepairsTab().getRepairsTable().getModel();

        HTMLBuilder htmlBuilder = new HTMLBuilder("Witaj, świecie!");

        htmlBuilder.writeHeadContent(
                    "<style>" +
                            "th { border-left: 2px solid #888 ; };" +
                    "</style>"
        );

        htmlBuilder.writeBodyContent("<table>");
        htmlBuilder.writeBodyContent("<tr>");
        for (int i = 0; i < repairsTableModel.getColumnCount(); i++) {
            htmlBuilder.writeBodyContent("<th>");
            htmlBuilder.writeBodyContent(repairsTableModel.getColumnName(i));
            htmlBuilder.writeBodyContent("</th>");
        }
        htmlBuilder.writeBodyContent("</tr>");
        htmlBuilder.writeBodyContent("</table>");

        // TODO: umożliwić użytkownikowi zapisanie pliku w dowolnej lokalizacji z okienka dialogowego
        htmlBuilder.save("naprawy.html");
    }
}
