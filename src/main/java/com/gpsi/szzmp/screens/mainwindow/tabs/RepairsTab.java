package com.gpsi.szzmp.screens.mainwindow.tabs;

import com.gpsi.szzmp.screens.mainwindow.RepairsTable;
import com.gpsi.szzmp.screens.mainwindow.tabs.exporting.RepairsTabExporter;

import javax.swing.*;
import java.awt.*;


public class RepairsTab extends Tab {
    private JPanel panel1;
    private RepairsTable repairsTable;

    public RepairsTable getRepairsTable() {
        return repairsTable;
    }

    public RepairsTab() {
        tabExporter = new RepairsTabExporter();

        setContentPane(panel1);

        repairsTable = new RepairsTable();
        repairsTable.setLayout(new GridLayout(0, 1));
        panel1.add(repairsTable);

        pack();
    }

    private void createUIComponents() {
        panel1 = new JPanel();
    }
}
