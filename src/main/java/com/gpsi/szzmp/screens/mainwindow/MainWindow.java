package com.gpsi.szzmp.screens.mainwindow;

import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.screens.mainwindow.tabs.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainWindow extends JFrame {

    private static MainWindow mainWindow;

    private JPanel panel1;

    private JTabbedPane tabbedPane1;
    private JButton exportViewButton;
    private JButton printViewButton;

    private OrdersTab ordersTab;
    private SearchTab searchTab;
    private RepairsTab repairsTab;
    private UsersTab usersTab;

    private List tabs;

    public List getTabs() {
        return tabs;
    }

    public OrdersTab getOrdersTab() {
        return ordersTab;
    }

    public SearchTab getSearchTab() {
        return searchTab;
    }

    public RepairsTab getRepairsTab() {
        return repairsTab;
    }

    public UsersTab getUsersTab() {
        return usersTab;
    }


    public static MainWindow getInstance() {
        return mainWindow;
    }

    private MainWindow() {
        super("System Zarządzania Zakładem Mechaniki Pojazdowej");
        setContentPane(panel1);

        ordersTab = new OrdersTab();
        searchTab = new SearchTab();
        repairsTab = new RepairsTab();
        usersTab = new UsersTab();
        tabs = new ArrayList<Tab>();
        tabs.add(ordersTab);
        tabs.add(searchTab);
        tabs.add(repairsTab);
        tabs.add(usersTab);

        tabbedPane1.add("Zamówienia", ordersTab.getContentPane());
        tabbedPane1.add("Wyszukiwanie", searchTab.getContentPane());
        tabbedPane1.add("Naprawy", repairsTab.getContentPane());
        tabbedPane1.add("Użytkownicy", usersTab.getContentPane());

        try {
            Image imgSave = ImageIO.read(getClass().getResource("/save_icon.png"));
            Image imgPrint = ImageIO.read(getClass().getResource("/print_icon.png"));
            exportViewButton.setIcon(new ImageIcon(imgSave));
            printViewButton.setIcon(new ImageIcon(imgPrint));
        } catch (IOException ex) {
        }

        exportViewButton.addActionListener(new ExportViewEventListener());
        printViewButton.addActionListener(new PrintViewEventListener());

        pack();
    }

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        mainWindow = new MainWindow();
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setVisible(true);

        mainWindow.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                DBConnection.cleanup();
                e.getWindow().dispose();
            }
        });
    }

    private void createUIComponents() {
        exportViewButton = new JButton();
        printViewButton = new JButton();
    }

    class ExportViewEventListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Tab selectedTab = (Tab) MainWindow.getInstance().getTabs().get(tabbedPane1.getSelectedIndex());
            selectedTab.getTabExporter().export();
        }
    }

    class PrintViewEventListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Tab selectedTab = (Tab) MainWindow.getInstance().getTabs().get(tabbedPane1.getSelectedIndex());
            System.out.println("Drukowanie odpowiedniej zakładki...");
        }
    }
}

