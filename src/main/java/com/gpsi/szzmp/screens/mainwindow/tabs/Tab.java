package com.gpsi.szzmp.screens.mainwindow.tabs;


import com.gpsi.szzmp.screens.mainwindow.tabs.exporting.TabExporter;

import javax.swing.*;

public abstract class Tab extends JFrame {
    protected TabExporter tabExporter;

    public TabExporter getTabExporter() {
        return tabExporter;
    }
}
