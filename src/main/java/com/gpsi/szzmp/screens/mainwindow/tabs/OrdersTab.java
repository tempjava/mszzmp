package com.gpsi.szzmp.screens.mainwindow.tabs;

import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.logic.IssueMapper;
import com.gpsi.szzmp.utils.comboitems.ComboItem;
import com.gpsi.szzmp.screens.mainwindow.tabs.exporting.OrdersTabExporter;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class OrdersTab extends Tab {
    private JPanel panel1;

    private JComboBox orderComboBox; // Zamowienie
    private JComboBox orderMakerComboBox; // Zamawiajacy
    private JFormattedTextField reservedDateFormattedTextField; // data
    private JComboBox componentComboBox; // czesc pojazdu
    private JComboBox issueComboBox; // problem

    private JButton storeOrderButton; // dodaj / edytuj
    private JButton deleteOrderButton; // usun

    // test

    ArrayList<ComboItem> components = new ArrayList<>();
    ArrayList<ComboItem> issues = new ArrayList<>();

    public void updateFormData() {
        orderComboBox.removeAllItems();

        try {
            Statement statement = DBConnection.connection.createStatement();
            orderComboBox.addItem("");
            ResultSet rs = statement.executeQuery("SELECT * FROM ORDERS LEFT JOIN USERS ON ORDERS.CLIENT_ID=USERS.USER_ID");
            while (rs.next()) {
                orderComboBox.addItem(new ComboItem("Nr klienta" + rs.getString("CLIENT_ID") + " Order id " + rs.getString("ORDER_ID"),
                        rs.getInt("ORDER_ID")));
            }

            if (orderComboBox.getSelectedItem() == null ||
                    orderComboBox.getSelectedItem().equals(""))
                return;

            // Ustawiam comboBoxy czesci pojazdu i problem
            componentComboBox.removeAllItems();
            for (ComboItem comboItem: IssueMapper.getComponentComboItems()) {
                componentComboBox.addItem(comboItem);
                components.add(comboItem);
            }

            issueComboBox.removeAllItems();
            for (ComboItem comboItem: IssueMapper.getIssueComboItems(((ComboItem) componentComboBox.getSelectedItem()).getValue())) {
                issueComboBox.addItem(comboItem);
                issues.add(comboItem);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public OrdersTab() {
        tabExporter = new OrdersTabExporter();

        setContentPane(panel1);
        pack();

        updateFormData();

        //Przy zmianie zamowienia odpala sie ta funkcja (inicjacja przez uzytkownika)
        // *takze po nacisnieciu przycisku Usuń Zamówienie (inicjacja w wyniku wyczyszczenia
        // danych przez metode updateFormData):
        orderComboBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                //Zczytuje nacisniety element

                System.out.println("string = |" + orderComboBox.getSelectedItem() + "|");

                // jezeli takiego elementu juz nie ma na liscie (np w wyniku (*)):
                if (orderComboBox.getSelectedItem() == null ||
                        orderComboBox.getSelectedItem().equals(""))
                    return;

                ComboItem userComboItem = ((ComboItem)orderComboBox.getSelectedItem());

                int order_id = userComboItem.getValue();
                try {
                    orderMakerComboBox.removeAllItems();
                    Statement statement = DBConnection.connection.createStatement();
                    String query = "SELECT * FROM ORDERS LEFT JOIN USERS ON ORDERS.CLIENT_ID=USERS.USER_ID WHERE ORDER_ID=" + order_id;
                    ResultSet rs = statement.executeQuery(query);
                    rs.next();
                    orderMakerComboBox.addItem(new ComboItem(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"), rs.getInt("CLIENT_ID")));
                    reservedDateFormattedTextField.setText(rs.getDate("APPOINTMENT_DATE").toString());
                    componentComboBox.setSelectedItem(components.get(rs.getInt("COMPONENT")));
                    issueComboBox.setSelectedItem(issues.get(rs.getInt("ISSUE")));
                } catch (SQLException sqle) {
                    System.err.print("Nie ma takiej pozycji.");
                }
            }
        });

        //SELECT * FROM ORDERS LEFT JOIN USERS ON ORDERS.CLIENT_ID=USERS.USER_ID WHERE ORDER_ID=1

        componentComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                issueComboBox.removeAllItems();
                for (ComboItem comboItem: IssueMapper.getIssueComboItems(((ComboItem) componentComboBox.getSelectedItem()).getValue()))
                    issueComboBox.addItem(comboItem);
            }
        });

        deleteOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Statement statement = DBConnection.connection.createStatement();
                    String query = "DELETE FROM ORDERS WHERE ORDER_ID=" +
                            ((ComboItem)orderComboBox.getSelectedItem()).getValue();
                    statement.execute(query);
                    updateFormData();

                }
                catch (SQLException sqle) {
                    System.out.print("NOPE");
                }
            }
        });

        storeOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }



}