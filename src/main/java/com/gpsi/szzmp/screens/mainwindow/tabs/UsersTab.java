package com.gpsi.szzmp.screens.mainwindow.tabs;

import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.screens.mainwindow.tabs.exporting.UsersTabExporter;
import com.gpsi.szzmp.utils.comboitems.StringComboItem;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;


public class UsersTab extends Tab {
    private JPanel panel1;
    private JComboBox usersComboBox;
    private JButton storeUserButton;
    private JTextField userLoginField;
    private JTextField userPhoneField;
    private JButton deleteUserButton;
    private JTextField userNameField;
    private JTextField userSurnameField;
    private JComboBox userRoleComboBox;

    public UsersTab() {
        tabExporter = new UsersTabExporter();

        setContentPane(panel1); // wybranie ramki z zawartością
        pack();

        storeUserButton.addActionListener(storeUserButtonListener);
        usersComboBox.addActionListener(usersComboBoxListener);
        deleteUserButton.addActionListener(deleteUserButtonListener);
        usersComboBox.addPopupMenuListener(usersComboBoxPopupMenuListener);
        userLoginField.setBackground(Color.lightGray);
    }

    private int roleToInt(String s){
        switch(s) {
            case "Klient":
                return 1;
            case "Mechanik":
                return 2;
            case "Kierownik":
                return 3;
        }
        return 0;
    }

    private UsersTab.StoreUserButtonListener storeUserButtonListener = new UsersTab.StoreUserButtonListener();
    private UsersTab.UsersComboBoxListener usersComboBoxListener = new UsersTab.UsersComboBoxListener();
    private UsersTab.UsersComboBoxPopupMenuListener usersComboBoxPopupMenuListener = new UsersTab.UsersComboBoxPopupMenuListener();
    private UsersTab.DeleteUserButtonListener deleteUserButtonListener = new UsersTab.DeleteUserButtonListener();

    class StoreUserButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //String name = ((JButton)e.getSource()).getText();
            //userNameField.setText(name);

            try {
                Statement statement = DBConnection.connection.createStatement();

                String query = "UPDATE USERS " +
                        "SET FIRST_NAME = '" + userNameField.getText() + "', " +
                        "LAST_NAME = '" + userSurnameField.getText() + "', " +
                        "PHONE = '" + userPhoneField.getText() + "', " +
                        "ROLE = " + roleToInt( (String) userRoleComboBox.getSelectedItem()) + " " +
                        "WHERE " + "LOGIN = '" + userLoginField.getText() + "'";
                System.out.println(query);
                statement.executeUpdate(query);

            } catch (Exception exception) {
                System.out.println(exception);
            }
        }
    }

    class  UsersComboBoxListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            StringComboItem userComboItem = ((StringComboItem)usersComboBox.getSelectedItem());
            if (userComboItem != null) {

                String login = (String) userComboItem.getValue();
                System.out.println("Kliknieto w ComboBox: " + (String) userComboItem.getValue());

                try {
                    Statement statement = DBConnection.connection.createStatement();

                    System.out.println("login = " + login);
                    ResultSet rs = statement.executeQuery("select * from USERS where LOGIN = '" + login + "'");
                    System.out.println("; ddd");

                    userNameField.setText(rs.getString("FIRST_NAME"));
                    userSurnameField.setText(rs.getString("LAST_NAME"));
                    userLoginField.setText(rs.getString("LOGIN"));
                    userPhoneField.setText(rs.getString("PHONE"));
                    userRoleComboBox.setSelectedIndex(Integer.parseInt(rs.getString("ROLE")));

                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        }
    }

    class UsersComboBoxPopupMenuListener implements PopupMenuListener {
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            System.out.println("Pokazano");

            usersComboBox.removeAllItems();

            try {
                Statement statement = DBConnection.connection.createStatement();

                ResultSet rs = statement.executeQuery("select * from USERS");
                while (rs.next())
                    usersComboBox.addItem(new StringComboItem(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"),
                            rs.getString("LOGIN")));

            } catch (Exception exception) {
                System.out.println(exception);
            }
        }

        public void popupMenuWillBecomeInvisible(PopupMenuEvent event) {
        }

        public void popupMenuCanceled(PopupMenuEvent event) {
        }

        public void actionPerformed(ActionEvent event) {
        }
    }

    class  DeleteUserButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Statement statement = DBConnection.connection.createStatement();

                String query = "DELETE FROM USERS WHERE LOGIN ='" + userLoginField.getText() + "'";
                clearUserInfoFields();
                System.out.println(query);
                statement.executeUpdate(query);

            } catch (Exception exception) {
                System.out.println(exception);
            }
        }
    }

    private void clearUserInfoFields(){
        userNameField.setText("");
        userSurnameField.setText("");
        userLoginField.setText("");
        userPhoneField.setText("");
        usersComboBox.setSelectedIndex(-1);
    }
}
