package com.gpsi.szzmp.screens;

import com.gpsi.szzmp.db.DBConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;


public class RegistrationWindow extends JFrame {
    private JTextField userFirstNameField;
    private JTextField userLastNameField;
    private JTextField userLoginField;
    private JTextField userPhoneField;
    private JButton registerButton;
    private JPanel rootPanel;
    private JPasswordField userPasswordField;
    private JLabel loginNotUniqueLabel;
    private JLabel validationErrorLabel;



    private RegisterButtonActionListener registerButtonListener = new RegisterButtonActionListener();

    public RegistrationWindow() {
        super("System Zarządzania Zakładem Mechaniki Pojazdowej");
        setContentPane(rootPanel);
        this.setResizable(false);
        pack();
    }

    public static void run() {
        RegistrationWindow registrationWindow = new RegistrationWindow();
        registrationWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        registrationWindow.centerLocation();
        registrationWindow.setVisible(true);
        registrationWindow.registerButton.addActionListener(registrationWindow.registerButtonListener);
    }


    private void centerLocation(){
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension screenSize = toolkit.getScreenSize();
        final int x = (screenSize.width - this.getWidth()) / 2; //these have to be adjusted
        final int y = (screenSize.height - this.getHeight()) / 3;
        this.setLocation(x, y);
    }



    private boolean validatePassword(){
        if(userPasswordField.getText().length() < 6) {
            validationErrorLabel.setText("Nie podano hasła lub jest ono zbyt krótkie (co najmniej 6 znaków)");
            validationErrorLabel.setVisible(true);
            pack();
            return false;
        }
        return true;
    }

    private boolean validateLogin(){
       if(userLoginField.getText().matches("^[A-Za-z0-9_-]{6,15}$"))
           return true;
        else {
           validationErrorLabel.setText("Wprowadzono niepoprawny login");
           validationErrorLabel.setVisible(true);
           pack();
           return false;
       }
    }

    private boolean validatePhone(){
        if(userPhoneField.getText().matches("^[\\s\\t]*$")){
            //empty field or field with whitespaces only should be valid;
            userPhoneField.setText(""); //
            return true;
        }
        else if (userPhoneField.getText().matches("^[0-9\\s\\+][0-9\\s]{8,}+$"))
            return true;
        else {
            validationErrorLabel.setText("Wprowadzono niepoprawny numer telefonu");
            validationErrorLabel.setVisible(true);
            pack();
            return false;
        }
    }

    private boolean validateLastName(){
        if(userFirstNameField.getText().matches("^[\\s\\t]*$")){
            //empty field or field with whitespaces only should be valid;
            userFirstNameField.setText(""); //
            return true;
        }
        else if (userFirstNameField.getText().matches("^[a-zA-Z]+[\\s]?[-]?[\\s]?[a-zA-Z]*$"))
            return true;

        else {
            validationErrorLabel.setText("Wprowadzono niepoprawne nazwisko");
            validationErrorLabel.setVisible(true);
            pack();
            return false;
        }
    }

    private boolean validateFirstName(){
        if(userFirstNameField.getText().matches("^[\\s\\t]*$")){
            //empty field or field with whitespaces only should be valid;
            userFirstNameField.setText(""); //
            return true;
        }
        else if (userFirstNameField.getText().matches("^[a-zA-Z]+$"))
            return true;

        else {
            validationErrorLabel.setText("Wprowadzono niepoprawne imię");
            validationErrorLabel.setVisible(true);
            pack();
            return false;
        }
    }

    private boolean validateAll(){
        return validateLogin() && validatePassword() && validateFirstName() && validateLastName() && validatePhone();
    }


    public class RegisterButtonActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            loginNotUniqueLabel.setVisible(false);
            validationErrorLabel.setVisible(false);

            if (validateAll()) {
                try {
                    Statement statement = DBConnection.connection.createStatement();
                    String query = "insert into USERS (LOGIN, PASSWORD, FIRST_NAME, LAST_NAME, PHONE, ROLE) " +
                            "values (" +
                            (userLoginField.getText().equals("") ? "null" : "'" + userLoginField.getText() + "'") +
                            ", " +
                            (userPasswordField.getText().equals("") ? "null" : "'" + userPasswordField.getText() + "'") +
                            ", '" +
                            userFirstNameField.getText() +
                            "', '" +
                            userLastNameField.getText() +
                            "', '" +
                            userPhoneField.getText() +
                            "', " +
                            1 +
                            ")";
                    System.out.println(query);
                    statement.executeUpdate(query);
                    //closing the window
                    setVisible(false);
                    dispose();
                } catch (SQLException sqlException) {
                    if (sqlException.getMessage().substring(0, 6).equals("UNIQUE")) {
                        loginNotUniqueLabel.setText("Podany login już istnieje");
                        loginNotUniqueLabel.setVisible(true);
                        pack();
                    }
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        }
    }
}