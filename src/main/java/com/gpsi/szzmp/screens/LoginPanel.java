package com.gpsi.szzmp.screens;

import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.screens.mainwindow.MainWindow;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

public class LoginPanel {

    private static JFrame frame;
    private static JLabel userLabel;
    private static JLabel infoLabel;
    private static JTextField userText;
    private static JLabel passwordLabel;
    private static JPasswordField passwordText;
    private static JButton loginButton;
    private static JButton registerButton;


    public class LoginButtonActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            boolean authenticated = false;

            try {
                Statement statement = DBConnection.connection.createStatement();

                ResultSet resultSet = statement.executeQuery("select * from USERS");

                while (resultSet.next()) {
                    if (userText.getText().equals(resultSet.getString("LOGIN")) &&
                            passwordText.getText().equals(resultSet.getString("PASSWORD"))) {
                        authenticated = true;
                    } else{
                        infoLabel.setForeground(Color.RED);
                        infoLabel.setText("Błędne dane");
                    }
                }

            } catch (Exception exception) {
            }

            if (authenticated) {
                MainWindow.run();
                frame.setVisible(false);
            }
        }
    }

    public class RegisterButtonActionListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                //RegistrationWindow registrationWindow = new RegistrationWindow();
                //registrationWindow.setVisible(true);
                RegistrationWindow.run();
            }
    }

    private LoginButtonActionListener loginButtonActionListener;
    private RegisterButtonActionListener registerButtonActionListener;

	public void run() {
		frame = new JFrame("Logowanie");
		frame.setSize(400, 150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.add(panel);
		placeComponents(panel);

        loginButtonActionListener = new LoginButtonActionListener();
        loginButton.addActionListener(loginButtonActionListener);

        registerButtonActionListener = new RegisterButtonActionListener();
        registerButton.addActionListener(registerButtonActionListener);
        frame.setResizable(false);
		frame.setVisible(true);
	}

	private static void placeComponents(JPanel panel) {

		panel.setLayout(null);

		userLabel = new JLabel("User");
		userLabel.setBounds(10, 10, 80, 25);
		panel.add(userLabel);

        infoLabel = new JLabel("Podaj dane");
        infoLabel.setBounds(290,35, 80, 25);
        panel.add(infoLabel);

		userText = new JTextField(20);
		userText.setBounds(100, 10, 160, 25);
		panel.add(userText);

		passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 40, 80, 25);
		panel.add(passwordLabel);

		passwordText = new JPasswordField(20);
		passwordText.setBounds(100, 40, 160, 25);
		panel.add(passwordText);

		loginButton = new JButton("Zaloguj");
		loginButton.setBounds(10, 80, 80, 25);
		panel.add(loginButton);
		
		registerButton = new JButton("Rejestracja");
		registerButton.setBounds(160, 80, 100, 25);
		panel.add(registerButton);
	}

}
