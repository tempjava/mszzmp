package com.gpsi.szzmp.startup;

import com.alee.laf.WebLookAndFeel;
import com.gpsi.szzmp.db.DBConnection;
import com.gpsi.szzmp.screens.LoginPanel;
import com.gpsi.szzmp.screens.mainwindow.MainWindow;

import javax.swing.*;

public class Startup {

    public static String getPath() {
        return "";
    }

    public static void main ( String[] args )
    {
        // You should work with UI (including installing L&F) inside Event Dispatch Thread (EDT)
        SwingUtilities.invokeLater (new Runnable ()
        {
            public void run ()
            {
                DBConnection.init();

                // Install WebLaF as application L&F

                WebLookAndFeel.install();

                // Create you Swing application here
                LoginPanel loginPanel = new LoginPanel();
                //loginPanel.run();

                MainWindow.run();
            }
        } );
    }
}
