package com.gpsi.szzmp.utils.htmlbuilder;


import java.io.*;

public class HTMLBuilder {

    private String headerStart = new String();
    private String headContent = new String();
    private String headerEnd = new String();
    private String footer = new String();
    private String bodyContent = new String();

    public HTMLBuilder(String title) {
        writeHeaderStart(title);
        writeHeaderEnd(title);
        writeFooter();
    }

    private void writeHeaderStart(String title) {
        headerStart +=
                "<!DOCTYPE html>" +
                        "<head>" +
                            "<title>" + title + "</title>" +
                            "<meta charset=\"utf-8\">";
    }

    private void writeHeaderEnd(String title) {
        headerEnd +=
                        "</head>" +
                        "<body>";
    }

    private void writeFooter() {
        footer +=
                "</body>" +
                        "</html>";
    }

    public void writeBodyContent(String newContent) {
        bodyContent += newContent;
    }

    public void writeHeadContent(String newContent) {
        headContent += newContent;
    }

    public void save(String path) {
        FileOutputStream fileOutputStream;

        try {
            fileOutputStream = new FileOutputStream(path);
        } catch (FileNotFoundException exception) {
            System.err.println("Nie znaleziono pliku " + exception + "!");
            return;
        }

        Writer out;

        try {
            out = new BufferedWriter(new OutputStreamWriter(fileOutputStream, "UTF-8"));
        } catch (UnsupportedEncodingException exception) {
            System.err.println("Nieobsługiwane kodowanie!");
            return;
        }

        try {
            out.write(headerStart + headContent + headerEnd + bodyContent + footer);
        } catch (IOException exception) {
            System.err.println("Jakiś straszny błąd z zapisywaniem!");
        } finally {
            try {
                out.close();
            } catch (IOException exception) {
                System.err.println("Nie udało się zamknąć pliku!");
            }
        }
    }
}
