package com.gpsi.szzmp.logic;


import com.gpsi.szzmp.utils.comboitems.ComboItem;

import java.util.ArrayList;
import java.util.List;

public class IssueMapper {

    public static ComponentType[] componentTypes = new ComponentType[] {
        new ComponentType("Koło", new IssueType[] {
                new IssueType("Przebita opona"),            // 2
                new IssueType("Niewłaściwe ciśnienie"),     // 3
                new IssueType("Wymiana opon")               // 5
        }),
        new ComponentType("Silnik", new IssueType[] {
                new IssueType("Wymiana oleju"),             // 8
                new IssueType("Wymiana świec")              // 1
        }),
        new ComponentType("Karoseria", new IssueType[] {
                new IssueType("Zarysowanie karoserii"),     // 1
                new IssueType("Wgniecenie karoserii"),      // 2
                new IssueType("Lakierowanie karoserii")     // 3
        }),
    };

    public static String getIssueTypeString(int componentIndex, int issueIndex) {
        return componentTypes[componentIndex].getIssueTypes()[issueIndex].getDescription();
    }

    public static String getComponentTypeString(int componentIndex) {
        return componentTypes[componentIndex].getName();
    }

    public static ComboItem[] getComponentComboItems() {
        List<ComboItem> comboItems = new ArrayList<>();
        int i = 0;
        for (ComponentType componentType : componentTypes)
            comboItems.add(new ComboItem(componentType.getName(), i++));

        return comboItems.toArray(new ComboItem[i]);
    }

    public static ComboItem[] getIssueComboItems(int componentIndex) {
        List<ComboItem> comboItems = new ArrayList<>();
        int i = 0;
        for (IssueType issueType : componentTypes[componentIndex].getIssueTypes())
            comboItems.add(new ComboItem(issueType.getDescription(), i++));

        return comboItems.toArray(new ComboItem[i]);
    }

    public static void main(String[] args) {
        // test
        System.out.println(IssueMapper.getIssueTypeString(1, 1));
    }
}
