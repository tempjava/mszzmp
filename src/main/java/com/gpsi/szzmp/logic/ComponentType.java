package com.gpsi.szzmp.logic;


class ComponentType {
    private String name;

    private IssueType[] issueTypes;

    ComponentType(String name, IssueType[] issueTypes) {
        this.name = name;
        this.issueTypes = issueTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IssueType[] getIssueTypes() {
        return issueTypes;
    }
}
