package com.gpsi.szzmp.logic;

class IssueType {
    private String description;

    IssueType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}