package com.gpsi.szzmp.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {

    public static Connection connection;
    public static Statement statement;
    public static ResultSet resultSet;

    public static void init() {

        try {
            Class.forName("org.sqlite.JDBC");
        }
        catch (Exception e) {
            System.err.println(e);
        }

        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:C:/db/szzmp.db");
        }
        catch(SQLException e)
        {
            System.err.println(e.getMessage());
        }
    }

    public static void cleanup() {
        try {
            if(connection != null)
                connection.close();
        }
        catch(SQLException e)  {
            System.err.println(e);
        }
    }

}
